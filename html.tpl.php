<?php // $Id$
/**
 * @file
 * The html around the main content. Includes styles and js.
 */
?>
<!DOCTYPE html>

<html lang="<?php print $language->language ?>" class="no-js">

<head>
  
  <meta charset="utf-8">
  <!-- www.phpied.com/conditional-comments-block-downloads/ -->
  <!--[if IE]><![endif]-->

  <?php print $head ?>

  <?php print $scripts ?>

  <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <title><?php print $head_title ?></title>
  <?php print $styles ?>
  <meta name="description" content="">
  <meta name="author" content="Cameron Little">
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">

  <link href='http://fonts.googleapis.com/css?family=Reenie+Beanie' rel='stylesheet' type='text/css'>

</head>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->

<!--[if lt IE 7 ]> <body class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <body class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <body class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <body class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <body> <!--<![endif]-->

<body class="<?php print $classes; ?>">
  <div id="skip-link" class="visuallyhidden">
    <a href="#main"><?php print t('Skip to main content'); ?></a>
  </div>

  <?php print $page_top ?>
  
  <?php print $page ?>
  
  <?php print $page_bottom ?>


  <!-- Javascript at the bottom for fast page loading -->
  <!--[if lt IE 7 ]>
    <script src="js/dd_belatedpng.js?v=1"></script>
  <![endif]-->
  <?php print $scripts ?>
  <script type='text/javascript'>
    (function ($) {
      
      $("#twitter").tweet({
        join_text: "auto",
        username: "<?php print theme_get_setting('twitter'); ?>",
        avatar_size: 32,
        count: 5,
        auto_join_text_default: "",
        auto_join_text_ed: "I",
        auto_join_text_ing: "I was",
        auto_join_text_reply: "I replied",
        auto_join_text_url: "I was checking out",
        loading_text: "loading tweets..."
      });
      
    })(jQuery);
  </script>

</body>

</html>