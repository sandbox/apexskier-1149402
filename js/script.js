/* Author: Cameron Little
*/

(function ($) {
	
	var tag_counts = new Array(); // Number of posts under each tag
  $(".view-id-tags .views-row span").each( function(i) {
  	tag_counts[i] = parseFloat( $(this).attr('alt') );
  });
  
  // var max_tag_count = Math.max.apply( Math, tag_count );
  // var min_tag_count = Math.min.apply( Math, tag_count );
  
  $('.view-id-tags').append(tag_counts);
  
	$('#block-user-login h3').click(function() {
		$('#block-user-login .content').slideToggle();
	});

})(jQuery);  
