<?php // $Id$
/**
 * @file
 * The code for a video node.
 *
 * A video node is a node with an id of "video". It has a video host
 * field that has a facebook, vimeo, or youtube option, a description field
 * and a URL field.
 * TO DO: remove video host field. Not neccessary if scanning the URL for host.
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
    
  <?php if ($teaser): ?>
    <h3 class="title"><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h3>
  <?php endif; ?>
  
  <div class="submitted">
    <em class="post-time"><?php print $date; ?></em> | <a class="permalink" href="<?php print $node_url; ?>">Permalink</a><?php if (render($content['field_tags'])): ?> | <span class="terms">Tagged with <?php print render($content['field_tags']); ?></span><?php endif; ?>
  </div>

  <div class="video<?php if ($node->field_description): print " descriptionyes"; endif; ?>">

    <article <?php print $content_attributes ?>>
    
      <?php
        // We hide the comments and links now so that we can render them later.
        hide($content['comments']);
        hide($content['links']);
      ?>
    
      <?php if ($node->field_video_host[$node->language][0]['value'] == "youtube"): ?>
        <iframe class="youtube-player" type="text/html" width="570" height="343" src="http://www.youtube.com/embed/<?php print str_replace("http://www.youtube.com/watch?v=", "", $node->field_video_url[$node->language][0]['value']); ?>" frameborder="0"></iframe>
      
      <?php elseif ($node->field_video_host[$node->language][0]['value'] == "vimeo"): ?>
        <iframe src="http://player.vimeo.com/video/<?php print str_replace("http://vimeo.com/", "", $node->field_video_url[$node->language][0]['value']); ?>" width="570" height="320" frameborder="0"></iframe>

      <?php elseif ($node->field_video_host[$node->language][0]['value'] == "facebook"): ?>
        <div class="facebook-video">
          <object width="570" height="320" >
            <param name="allowfullscreen" value="true" />
            <param name="movie" value="http://www.facebook.com/v/<?php print str_replace("http://www.facebook.com/video/video.php?v=", "", $node->field_video_url[$node->language][0]['value']); ?>" />
            <embed src="http://www.facebook.com/v/<?php print str_replace("http://www.facebook.com/video/video.php?v=", "", $node->field_video_url[$node->language][0]['value']); ?>" type="application/x-shockwave-flash" allowfullscreen="true" width="570" height="320">
            </embed>
          </object>
        </div>
      <?php endif; ?>
      
      <?php if (!$teaser): ?>
        <div class="description"><?php print $node->field_description[$node->language][0]['value']; ?></div>
      <?php endif; ?>
      
    </article>

  </div>

  <?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
  ?>
    <div class="link-wrapper">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

  <?php print render($content['comments']); ?>

</div>
  