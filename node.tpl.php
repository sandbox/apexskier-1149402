<?php // $Id$
/**
 * @file
 * Code for a node
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
    
  <?php if ($teaser): ?>
    <h3 class="title"><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h3>
  <?php endif; ?>
  
  <?php if ($node->type !== "page"): ?>
    <div class="submitted">
      <em class="post-time"><?php print $date; ?></em> | <a class="permalink" href="<?php print $node_url; ?>">Permalink</a><?php if (render($content['field_tags'])): ?> | <span class="terms">Tagged with <?php print render($content['field_tags']); ?></span><?php endif; ?>
    </div>
  <?php endif; ?>

  <article <?php print $content_attributes ?>>
  
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>
  
  </article>

  <?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
  ?>
    <div class="link-wrapper">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

  <?php print render($content['comments']); ?>

</div>
