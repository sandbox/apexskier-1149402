<?php // $Id$
/**
 * @file
 * The code for a normal page.
 */
?>
  <div class="container clearfix">

  <nav>
    <?php print theme('links__system_main_menu', array('links' => $main_menu )); ?>
  </nav>

  <div id="twitter">
    <div class="twitter-bird"></div>
    <h2>@<a href="http://twitter.com/
    <?php
    $twitter_handle = theme_get_setting('twitter'); 
    print $twitter_handle;
    ?>"><?php print $twitter_handle; ?></a></h2>
  </div>

  <?php if ($is_front): ?>
    <div class="photo">
      <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"><img src="<?php print $logo; ?>" alt="<?php print t('Photo'); ?>" /></a>
      <?php endif; ?>
    </div>
  <?php endif; ?>

  <header class="<?php if ($is_front){ print "frontpage"; } ?>">
    <h1 class="name"><a href="<?php print $front_page ?>"><?php print $site_name ?></a></h1>
    <?php print render($page['header']) ?>
  </header>

  <?php if ($is_front): ?>
    <div id="gallery">
      <?php $videourl = theme_get_setting('featured_video'); ?>
      <?php if (strpos($videourl, "youtube")): ?>
        <iframe class="youtube-player" type="text/html" width="570" height="370" src="http://www.youtube.com/embed/<?php print str_replace("http://www.youtube.com/watch?v=", "", $videourl); ?>" frameborder="0"></iframe>
      <?php elseif (strpos($videourl, "vimeo")): ?>
        <iframe src="http://player.vimeo.com/video/<?php print str_replace("http://vimeo.com/", "", $videourl); ?>" width="570" height="370" frameborder="0"></iframe>
      <?php elseif (strpos($videourl, "facebook")): ?>
        <object width="550" height="370" >
          <param name="allowfullscreen" value="true" />
          <param name="movie" value="http://www.facebook.com/v/<?php print str_replace("http://www.facebook.com/video/video.php?v=", "", $videourl); ?>" />
          <embed src="http://www.facebook.com/v/<?php print str_replace("http://www.facebook.com/video/video.php?v=", "", $videourl); ?>" type="application/x-shockwave-flash" allowfullscreen="true" width="570" height="370">
          </embed>
        </object>
      <?php endif; ?>
    </div>
    <div id="sponsors-box"><?php print render($page['highlighted']) ?></div>
  <?php endif; ?>

  <section id="main">
  
    <?php if($title): ?><h2 class="title"<?php print ($tabs ? 'with-tabs"' : 'no-tabs"'); ?>><?php print $title ?></h2><?php endif ?>
    
    <?php if($tabs):
      print '<div class="tab-spacer-top"></div>';
      print render($tabs);
      print '<div class="tab-spacer"></div>';
    endif ?>
    
    <?php print render($page['help']) ?>
    
    <?php print $messages ?>
    
    <div id="content" <?php if ($tabs){ print 'class="with-tabs"'; }; ?>>
      <?php print render($page['content']) ?>
    </div>

    <div class="feed-container"><?php print $feed_icons ?></div>

  </section>
  
  <asside id="sidebar" class="clearfix">
  
    <?php print render($page['right']) ?>
    
  </asside>
    
  <footer>
    <?php print render($page['footer']) ?>      
  </footer>
  
</div>    