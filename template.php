<?php // $Id$
/**
 * @file
 * template.php file. Custom php functions.
 *
 * Customizes the rss feed icon.
 */

function skiertheme_feed_icon($variables) {
  $text = t('Subscribe to @feed-title', array('@feed-title' => $variables['title']));
  return l(t('Feed'), $variables['url'], array('html' => TRUE, 'attributes' => array('class' => array('feed-icon'), 'title' => $text)));
}