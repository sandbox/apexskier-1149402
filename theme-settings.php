<?php // $Id$

/**
 * @file
 * The theme settings.
 *
 * These settings allow for user submitted variables for things
 * such as the twitter handle and featured video.
 */

function skiertheme_form_system_theme_settings_alter(&$form, $form_state) {


/*
  $form['custom_background'] = array(
    '#type' => 'fieldset',
    '#title' => t('Background Image'),
    '#description' => t("Background image for site.")
  );
  $form['custom_background']['use_custom_background'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use a custom background image.'),
    '#default_value' => 0,
  );
  $form['custom_background']['custom_background_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to background image.'),
    '#default_value' => 'null',
  );

  $form['custom_background']['custom_background_upload'] = array(
    '#type' => 'file',
    '#title' => t('Upload background image'),
  );
*/







  // fieldgroups
  $form['socialmedia'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Social Media Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  // Social media elements
  $form['socialmedia']['twitter'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter handle'),
    '#default_value' => theme_get_setting('twitter')
  );
  $form['socialmedia']['featured_video'] = array(
    '#type' => 'textfield',
    '#title' => t('Featured Video URL'),
    '#default_value' => theme_get_setting('featured_video')
  );

  $form['#submit'][]   = 'skiertheme_settings_submit';
  return $form;
}

function skiertheme_settings_submit($form, &$form_state) {
  $settings = array();
}

/**
 * Check if folder is available or create it.
 *
 * @param <string> $dir
 *    Folder to check
 */
function _skiertheme_check_dir($dir) {
  // Normalize directory name
  $dir = file_stream_wrapper_uri_normalize($dir);

  // Create directory (if not exist)
  file_prepare_directory($dir,  FILE_CREATE_DIRECTORY);
}